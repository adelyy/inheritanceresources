require 'rails'
Gem.loaded_specs['inheritance'].dependencies.each do |d|
 require d.name
end

[
  'inheritance_role',
  'inheritance_user',
  'inheritance_permission'
].each do |concern|
  path = File.join(File.expand_path('../../..', __FILE__),
      "app/models/concerns/#{concern}.rb")
  require path
end

module Inheritance
  module Rails
    class Engine < ::Rails::Engine

      initializer 'inheritance_engine.include_concerns' do
        reloader_class.to_prepare do
          ['User', 'Role', 'Permission'].each do |model|
            begin
            model.constantize.send(:include, "Inheritance#{model}".constantize)
            rescue 
              puts "No existe el modelo #{model}. Ejecuta rails generate inheritance:install\n"
            end
          end
          ActiveRecord::Base.send(:include, InheritanceFilter)
          ActiveRecord::Base.send(:include, InheritanceApi)
          ActiveModelSerializers.config.adapter
        end
      end


      protected

        def reloader_class
          if defined? ActiveSupport::Reloader
            ActiveSupport::Reloader
          else
            ActionDispatch::Reloader
          end
        end
    end
  end
end
