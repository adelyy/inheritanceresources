require 'rails/generators'

module Inheritance
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      include ::Rails::Generators::Migration
      source_root File.expand_path('../templates', __FILE__)
      desc "add the migrations and models"

      def self.next_migration_number(path)
        unless @prev_migration_nr
          @prev_migration_nr = Time.now.utc.strftime("%Y%m%d%H%M%S").to_i
        else
          @prev_migration_nr += 1
        end
        @prev_migration_nr.to_s
      end

      def copy_models
        files = [
          'app/models/user.rb',
          'app/models/role.rb',
          'app/models/permission.rb']
        files.each do |file|
          if File.exist? file
            content = File.read(file)
            file_name = file.split('/').last.split('.').first.classify
            unless content.match(/Inheritance#{file_name}*$/)
              insert_text = "  include Inheritance#{file_name}\n"
              insert_into_file file, insert_text, after: "ActiveRecord::Base\n"
            end
          end
          copy_file file.split('/').last, file
        end
      end

      def copy_migrations
        copy_migration("create_roles")
        copy_migration("create_permissions")
        copy_migration("create_users")
      end

      def copy_test
        dir = 'spec'
        directory dir, 'spec', mode: :preserve
      end

      protected

        def copy_migration(filename)
          if self.class.migration_exists?("db/migrate", "#{filename}")
            say_status("skipped", "Migration #{filename}.rb already exists. COD[I001]")
          end
          migration_template "#{filename}.rb", "db/migrate/#{filename}.rb"
        end
    end
  end
end
