class User < ActiveRecord::Base
  validates :username, :password, :role, :first_name, :last_name, presence: true
end
