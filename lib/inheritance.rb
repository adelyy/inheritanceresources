module Inheritance
  module Rails
    require 'inheritance/engine' if defined?(Rails)
  end
end

require 'inheritance/inheritance' if defined?(Rails)
