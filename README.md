# Inheritance



## Instalar Gema


```ruby

  gem 'inheritance', git: 'git@bitbucket.org:adelyy/inheritanceresources.git', tag: 'v1.0'

```

Versiones disponibles:
  
  - version 1.0 tag: 'v1.0': Inheritance Controller
  - version en  `desarrollo` tag: `N/A`: InheritanceController and AccessController



## Generar modelos de usuarios y roles:

```ruby

  rails generate inheritance:install

```


## ¿Como usar Inheritance Controller?

Para usar InheritanceController en tu controlador remplaza `ApplicationController` por `Inheritance::ControllerBase`

```ruby

class Admin::Users < Inheritance::ControllerBase

```

Para utilizar AccessController junto con InheritanceController reemplaza `ApplicationController` por `Inheritance::ControllerAccess`

```ruby

class Admin::Users < Inheritance::ControllerAccess

```

