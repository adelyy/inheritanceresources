module InheritanceFilter
  extend ActiveSupport::Concern

  included do
    after_create :set_order
  end

  def filter_name
    respond_to?(:name) ? name : send(attribute_names[1].to_sym)
  end

  def set_order
    if self.class.ordered?
      update(order_number: (self.class.maximum(:order_number) || 0) + 1)
    end
  end
  
  module ClassMethods
    def associations
      reflections.collect { |a, b| b.class_name if b.macro == :belongs_to }
        .compact.map { |x| x.constantize }
    end

    def all_ordered
      if incorrect_order_structure?
        order_by_default
      end
      all.order("order_number")
    end

    def ordered?
      attribute_names.include? 'order_number'
    end

    def order_by_default
      if ordered?
        all.each_with_index { |obj, index| obj.update(order_number: index + 1) }
      else
        puts 'this class is not prepared to be ordered'
      end
    end

    def incorrect_order_structure?
      all.pluck(:order_number).include? nil
    end

    def permitted_params
      attribute_names - ["id", "created_at", "updated_at", "order_number"]
    end

    def listed_params
      attribute_names - ["id", "created_at", "updated_at"]
    end
  end
end
