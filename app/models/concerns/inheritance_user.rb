module InheritanceUser
  extend ActiveSupport::Concern
  included do
    belongs_to :role
    has_many :permissions, through: :role
    has_secure_password
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def filter_name
    full_name
  end

  def update_token
    update(token: User.create_token)
  end

  def can(model, ability)
    p = Hash[Permission.abilities.zip(0..Permission.abilities.size)]
    assigments = permissions.find_by(controller: model).assignment
    assigments.include? p[ability.to_sym] if assigments
  end

  module ClassMethods
    def create_token
      begin
        token = SecureRandom.base64(32)
      end while(User.exists?(token: token))
      token
    end

    def filter_attributes
      [ :username ]
    end

    def permitted_params
      [ :username, :first_name, :last_name, 
        :role_id, :password, :password_confirmation ]
    end

    def listed_params
      [ :username, :full_name ]
    end
  end
end
