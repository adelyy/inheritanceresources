module InheritancePermission
  extend ActiveSupport::Concern
  included do
    belongs_to :role, optional: true
    validates :controller, presence: true
  end
  module ClassMethods
    def permissions
      permissions = abilities
      Hash[permissions.map do |p| 
        "<span>#{human_attribute_name(p)}</span>".html_safe
      end.zip(0..permissions.size)]
    end

    def abilities
      [:show, :create, :update, :destroy]
    end

    def i18n_abilities
      abilities.map { |a| human_attribute_name(a) }
    end  
  end
end
