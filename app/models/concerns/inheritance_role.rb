module InheritanceRole
  extend ActiveSupport::Concern
  included do
    has_many :permissions, dependent: :destroy
    has_many :users, dependent: :destroy
    accepts_nested_attributes_for :permissions, reject_if: :all_blank, allow_destroy: true
  end

  module ClassMethods
    def filter_attributes
      [ :name ]
    end

    def permitted_params
      [ :name ]
    end

    def controllers
      InheritanceController.descendants
    end

    def black_list_models
      ['Permission', 'ApplicationRecord']
    end

    def models
      Dir[File.join(Rails.root,"app/models","*.rb")].map do |f|
        model_name = File.basename(f,'.rb').classify
        model_name.constantize unless black_list_models.include? model_name
      end.compact
    end

    def model_names
      Role.models.map { |model| model.model_name.human count: 2} 
    end 
  end
end
