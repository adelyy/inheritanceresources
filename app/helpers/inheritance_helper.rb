module InheritanceHelper
  def get_path object
    object = action ? object.class : object
    polymorphic_path([namespace.to_sym, object], {locale: params[:locale]})
  end

  def set_order_url
    "#{namespace}/#{object_class.name.pluralize.underscore}/set_order"
  end

  def inherited_controller?
    "#{params[:controller].split("/").map { |x| x.split('_').map(&:capitalize)
      .join }.join('::')}Controller"
      .constantize < Inheritance::ControllerBase
  end

  def namespace
    namespace = params[:controller].split('/')
    namespace.count > 1 ? namespace.first : nil
  end

  def namespace_login_path
    if namespace.nil?
      send("login_path")
    else
      send("admin_login_path")
    end
  end

  def default_selected_value(param, object)
    params.keys.include?(param.to_s) ? params[param] : object.send(param)
  end

  def sort_column
    object_class.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

  def sortable(column, sorting=true)
    title = object_class.human_attribute_name(column)
    column = column.to_s
    css = column == sort_column ? "current #{sort_direction}" : nil
    direction = !css.nil? && sort_direction == 'asc' ? 'desc' : 'asc'
    sorting_options = sorting ? {sort: column, direction: direction} : nil
    link_to title, current_path(sorting_options), class: css
  end

  def current_path(params)
    path = request.fullpath.split('?').first
    params.each_with_index do |param, i|
      path += "#{i.zero? ? '?' : '&'}#{param[0]}=#{param[1]}"
    end
    return path
  end

  def uri_params(new_params)
    request.GET.merge new_params
  end

  def delete_button(object, message = nil)
    message ||= "El #{object.model_name.human} se eliminará, ¿deseas continuar?"
    link_to polymorphic_path([namespace.to_sym, object], {locale: params[:locale]}),
            method: :delete, data: { confirm: message },
            class: 'btn btn-danger' do
      "<span class='fa fa-minus' aria-hidden='true'
      title='Eliminar'></span>".html_safe
    end
  end

  def custom_button(object, options = nil)
    options[:icon_class] ||= "glyphicon-retweet"
    show = (object.class.method_defined? :show?)? object.send(:show?) : true
    link_to eval(options[:path]), method: options[:method], data: options[:data],
    remote: options[:remote], class: options[:class] do
      raw("<span class='fa #{options[:icon_class]}' aria-hidden='true' "+
      "title='#{options[:text]}'></span>")
    end if show
  end

  def edit_button(object)
    link_to edit_polymorphic_path([namespace.to_sym, object], {locale: params[:locale]}), class: 'btn btn-success' do
      "<span class='fa fa-edit' aria-hidden='true'
      title='Editar'></span>".html_safe
    end
  end

  def view_button(object)
    link_to polymorphic_path(namespace.to_sym, object, {locale: params[:locale]}), class: 'btn btn-info' do
      "<span class='fa fa-eye' aria-hidden='true'
      title='Ver'></span>".html_safe
    end
  end

  def td_link(link, text, css=nil)
    content_tag(:td, class: "no-selectable {css}", data: { link: link }) do
      text
    end
  end



  def permission_headers
    content_tag :div, class: 'header' do
      concat label_tag(Permission.human_attribute_name('controller'), nil, 
        class: 'check_boxes')
      Permission.i18n_abilities.each do |ability|
        concat label_tag(ability, nil, class: 'title')
      end
    end
  end

  def additional_information(parent_object, child_class, attributes, redirect_url = nil, shown_attributes_xs = [0], nested_path = true)
    content_tag(:div, class: 'panel panel-visible') do
      content_tag(:div, class: 'panel-heading') do
        content_tag(:div, class: 'panel-title') do
          content_tag(:span, nil, class: 'fa fa-user') +
          (child_class.model_name.human count: 2).capitalize +
          content_tag(:span, class: 'panel-controls') do
            link_to '', new_polymorphic_path([namespace.to_sym, (nested_path ? parent_object : nil), child_class.new]), class: 'panel-control-collapse'
          end
        end
      end +
      content_tag(:div, class: 'panel-body pn') do
        content_tag(:table, class: 'table table-striped table-hover dataTable no-footer') do
          content_tag(:thead) do
            content_tag(:tr) do
              # Headers
              attributes.each_with_index do |attr, i|
                concat content_tag(:th, child_class.human_attribute_name(attr), class: shown_attributes_xs.include?(i) ? '' : 'hidden-xs')
              end
              concat content_tag(:th, nil)
            end
          end +
          content_tag(:tbody) do
            parent_object.send(child_class.model_name.route_key).each_with_index do |child, i|
              concat (
                content_tag(:tr, class: "#{i.even? ? 'even' : 'odd'}") do
                  # Attributes
                  attributes.each_with_index do |attr, i|
                    concat td_link(edit_polymorphic_path([namespace.to_sym, (nested_path ? parent_object : nil), child]), child.send(attr))
                  end
                  # Links
                  concat(content_tag(:td) do
                      link_to polymorphic_path([namespace.to_sym, (nested_path ? parent_object : nil), child]), method: :delete, data:
                        { confirm: "El #{child.model_name.human} se eliminará, ¿deseas continuar?" },
                        class: '' do
                          content_tag(:span, nil, class: 'fa fa-remove float-right')
                        end
                    end)
                end)
            end
          end
        end
      end
    end
  end

  def filter_select(associated_models, attributes=[], path=request.original_url)
    return nil if associated_models.empty? and attributes.empty?
    form_tag path, class: 'filters', method: :get do
      associated_models.each do |model|
        concat(
          select_tag "filter_model[#{model.to_s.downcase}_id]",
          options_from_collection_for_select(model.all, "id", "filter_name"),
          multiple: true,
          data: { type: 'multipleselect', model: model.to_s.downcase,
              title: model.model_name.human, nselected: t('menu.selected'),
                allselected: t("activerecord.models.#{model.to_s.downcase}.all_selected") })
      end
      attributes.each do |attr|
        concat(
          select_tag "filter_attribute[id]",
          options_from_collection_for_select(@objects, "id", attr),
          multiple: true,
          data: { type: 'multipleselect', model: object_class.to_s.downcase,
            attribute: attr.to_s, title: object_class.human_attribute_name(attr),
              nselected: t('menu.selected'),
                allselected: t("activerecord.models.#{object_class.to_s.downcase}.all_selected") })
      end
      concat submit_tag t('menu.filter').capitalize, class: 'btn btn-dark btn-controls'
      concat link_to t('menu.clear_filter'),
        request.original_url.split('?').first, class: 'btn btn-info btn-controls'
    end
  end
end
