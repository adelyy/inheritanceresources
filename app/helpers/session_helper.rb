module SessionHelper
  def user_logged_in?
    current_user.persisted?
  end

  def current_user
    User.find_by!(id: session[:user_id]) rescue User.new
  end

  def index_path(object)
    polymorphic_path([namespace.to_sym, object], {locale: params[:locale]})
  end

  def menu_link(object, icon, action=:show)
    if current_user.can(object.name, action)
      name = object.model_name.human count: 2
      sidebar_icon_link(name, index_path(object), icon, nil)
    end
  end

  def user_accordion(title, icon, &block)
    sidebar_icon_accordion(title, icon, yield) if yield
  end

  def accordion_link(object, icon, action=:show)
    name = object.model_name.human count: 2
    accordion_icon_link name, index_path(object), icon, nil
  end
end
