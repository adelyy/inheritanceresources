module ApiHelper
  def token
    request.headers['X-Api-Key']
  end

  def current_user
    @current_user ||= User.find_by!(token: token) rescue User.new
  end

  def user_logged_in?
    current_user.persisted?
  end

  def action2ability
    {
      'index' => 'show',
      'edit' => 'update',
      'new' => 'create'
    }[params[:action]] || params[:action]
  end

end
