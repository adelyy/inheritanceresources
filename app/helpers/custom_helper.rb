module CustomHelper
   def action2ability
    {
      'index' => 'show',
      'edit' => 'update',
      'new' => 'create'
    }[params[:action]] || params[:action]
  end
end
