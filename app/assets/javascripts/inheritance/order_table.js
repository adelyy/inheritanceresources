var fixHelperModified = function(e, tr) {
  var $originals = tr.children();
  var $helper = tr.clone();
  $helper.children().each(function(index) {
    $(this).width($originals.eq(index).width());
  });
  return $helper;
},

updateIndex = function(e, ui) {
  newOrder = [];
  table = $('td.index').parent().parent().parent();
  $('td.index', ui.item.parent()).each(function (i) {
    $(this).html(i + 1);
    newOrder.push([$(this).data('id'), i + 1])
  });
  
  requestupd(newOrder, table.data('order-url'));
};

function requestupd(newOrder, url) {
  var arr = window.location.href.split("/");
  var request_url = arr[0] + "//" + arr[2] + "/" + url + "?p=" + newOrder
  console.log(request_url);
  $.ajax({
    type: 'GET',
    url: request_url,
  }).done(function(data) {
    console.log(data);
  });
}

function requestudp() {
  $.ajax({
    url: "test.html",
    context: document.body
  }).done(function() {
    $( this ).addClass( "done" );
  });
}

$(document).ready(function() {
  $(".table.sort tbody").sortable({
    helper: fixHelperModified,
    stop: updateIndex
  }).disableSelection();
});
