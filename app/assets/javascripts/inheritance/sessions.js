//= require bitlab/plugins/canvasbg/canvasbg

$( window ).resize(calculatePosition);
$( document ).ready(function(){
  if ($('.login-form').length) {
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });
    calculatePosition();
  }
});

function calculatePosition () {
  $('.login-form').css('left', ($(window).width() - $('.login-form').width())/2)
  $('.login-form').css('top', ($(window).height() - $('.login-form').height())/2)
}
