$( document ).ready(function(){
  $('*[data-type="multipleselect"]').each(function(){
    $( this ).multiselect({
      enableFiltering: true,
      includeSelectAllOption: true,
      enableCaseInsensitiveFiltering: true,
      nonSelectedText: $(this).data('title'),
      nSelectedText: $(this).data('nselected'),
      selectAllText: 'Seleccionar todas',
      allSelectedText: $(this).data('allselected'),
    });
  });
});
