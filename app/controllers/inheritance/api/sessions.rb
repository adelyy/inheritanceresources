module Inheritance
  module Api
    class Sessions < ActionController::API
    include ActionController::Serialization

      def create
        user = User.find_by_username(params[:username]) || User.new
        if user.persisted? && user.authenticate(params[:password])
          user.update_token
          render json: user, status: :ok
        else
          render json: {}, status: :unauthorized
        end
      end

      def destroy
        user = User.find_by(token: request.headers['X-Api-Key'])
        user.update_token
        render json: {}, status: :ok
      end
    end
  end
end
