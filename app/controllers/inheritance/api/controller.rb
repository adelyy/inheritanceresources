module Inheritance
  module Api
    class Controller < ActionController::API
    include ActionController::Serialization
    include ApiHelper
    helper_method :object_class, :current_user
    before_action :require_login
    before_action :require_ability

    def index
      query_models = filter_params(:filter_model)
      query_attributes = filter_params(:filter_attribute)
      query = (query_attributes || {}).merge(query_models || {})
      @objects = object_class.where(query)
      .order("#{params[:sort]} #{params[:direction]}")
      render json: @objects, status: :ok
    end

    def show
      @object = find_object
      render json: @object, status: :ok
    end

    def create(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      @object = object_class.new(permitted_params)
      if @object.save
        render json: @object, status: :created
      else
        render json: @object, status: :bad_request
      end
    end

    def update(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      @object = find_object
      if @object.update(permitted_params)
        render json: @object, status: :created
      else
        render json: @object, status: :bad_request
      end
    end

    def destroy(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      find_object.destroy
      render json: {}, status: :ok
    end

    private

    def filter_params(filter)
      params.require(filter)
      .permit(Hash[params[filter].keys
        .zip [[]]*params[filter].size]) if params.include? filter
    end

    def find_object
      controller_name.classify.constantize.find(params[:id])
    end

    def find_class
      controller_name.classify.constantize
    end

    def sort_column
      object_class.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end

    def object_class
      controller_name.classify.constantize
    end

    def permitted_params
      params.require(params[:controller].classify
        .parameterize.to_sym).permit(find_class.permitted_params)
    end

    def require_login
      unless user_logged_in?
        render json: {}, status: :forbidden 
      end
    end

    def require_ability
      unless current_user.can(object_class.name, action2ability)
        render json: {}, status: :unauthorized 
      end
    end
  end
  end
end
