module Inheritance
  class ControllerAccess < ControllerBase
    include SessionHelper
    include InheritanceHelper
    include CustomHelper
    before_action :require_login
    before_action :require_ability

    def require_login
      unless user_logged_in?
        redirect_to namespace_login_path
      end
    end

    private

    def require_ability
      unless current_user.can(object_class.name, action2ability)
        render file: "public/401.html", status: :unauthorized 
      end
    end
  end
end
