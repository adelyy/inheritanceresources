module Inheritance
  class RolesController < ControllerAccess
  layout 'admin'

  def new
    @object = object_class.new
    @object.permissions.build
    generate_model_list
  end

  def create
    redirect_path = polymorphic_path([namespace.to_sym, object_class],
      {locale: params[:locale]})
    @object = object_class.new(permitted_params)
    if @object.save
      redirect_to redirect_path
    else
      @object.permissions.build
      @model_list = []
      respond_with @object
    end
  end

  def edit
    super
    generate_model_list
  end

  def update
    super
    generate_model_list
  end

  def listed_params
    [ :name ]
  end

  private

  def generate_model_list
    c = @object.permissions.pluck(:controller)
    @model_list = Role.models.delete_if { |m| c.include? m.name }
  end

  def permitted_params
    params.require(:role).permit(:name, permissions_attributes: [ :id, :controller, assignment:[]])
  end
end

end
