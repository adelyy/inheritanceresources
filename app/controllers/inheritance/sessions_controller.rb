module Inheritance
  class SessionsController < ApplicationController
    include SessionHelper
    include InheritanceHelper
    include CustomHelper
    layout 'sessions'

    def new
      redirect_to session_paths(current_user.role.name) if user_logged_in?
    end

    def create
      user = User.find_by_username(params[:username]) || User.new
      if user.persisted? && user.authenticate(params[:password])
        session[:user_id] = user.id
        redirect_to session_paths(user.role.name)
      else
        flash[:error] = 'Usuario y/o contraseña inválida'
        redirect_to namespace_login_path
      end
    end

    def destroy
      session[:user_id] = nil
      redirect_to namespace_login_path
    end
  end

end
