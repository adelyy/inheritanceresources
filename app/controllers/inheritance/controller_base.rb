module Inheritance
  class ControllerBase < ApplicationController
    include InheritanceHelper
    helper_method :object_class, :sort_column, :sort_direction
    before_action :order_verify
    respond_to :html

    def index
      order_query = (object_class.ordered? and params[:sort].nil?) ?
        "order_number" : "#{params[:sort]} #{params[:direction]}"
      query_models = filter_params(:filter_model)
      query_attributes = filter_params(:filter_attribute)
      query = (query_attributes || {}).merge(query_models || {})
      @objects = object_class.where(query)
      .order(order_query)
    end

    def show
      @object = find_object
    end

    def edit
      @object = find_object
    end

    def new
      @object = object_class.new
    end

    def create(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      @object = object_class.new(permitted_params)
      if @object.save
        redirect_to redirect_path
      else
        respond_with @object
      end
    end

    def update(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      @object = find_object
      if @object.update(permitted_params)
        redirect_to redirect_path
      else
        respond_with @object
      end
    end

    def destroy(redirect_path = nil)
      redirect_path ||= polymorphic_path([namespace.to_sym, object_class],
        {locale: params[:locale]})
      find_object.destroy
      redirect_to redirect_path
    end

    def set_order
      if object_class.ordered?
        new_ids = params[:p].split(",").each_slice(2).to_a
        new_ids.each do |object|
          object_class.find(object[0]).update(order_number: object[1])
        end
        render json: { message: "#{object_class.name} successfully ordered" }
      else
        render json: { error: "#{object_class.name} is not prepared to be ordered" }
      end
    end

    private

    def filter_params(filter)
      params.require(filter)
      .permit(Hash[params[filter].keys
        .zip [[]]*params[filter].size]) if params.include? filter
    end

    def order_verify
      if object_class.ordered? and !object_class.count.zero? and
        object_class.incorrect_order_structure?
        object_class.order_by_default
      end
    end

    def find_object
      @object ||= controller_name.classify.constantize.find(params[:id])
    end

    def find_class
      controller_name.classify.constantize
    end

    def sort_column
      object_class.column_names.include?(params[:sort]) ? params[:sort] : "id"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end

    def object_class
      controller_name.classify.constantize
    end

    def permitted_params
      params.require(controller_name
        .singularize.to_sym).permit(find_class.permitted_params)
    end
  end
end
