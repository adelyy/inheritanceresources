# -*- encoding: utf-8 -*-
# stub: inheritance 0.0.2 ruby lib

Gem::Specification.new do |s|
  s.name = "inheritance"
  s.version = "0.0.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = [""]
  s.date = "2018-12-21"
  s.description = "Inheritance Controller, Helpers and Full role users"
  s.email = [""]
  s.files = ["MIT-LICENSE", "README.rdoc", "Rakefile", "app/assets/images/login_background.jpg", "app/assets/javascripts/inheritance", "app/assets/javascripts/inheritance/filter.js", "app/assets/javascripts/inheritance/order_table.js", "app/assets/javascripts/inheritance/sessions.js", "app/assets/stylesheets/inheritance", "app/assets/stylesheets/inheritance/default.scss", "app/assets/stylesheets/inheritance/sessions.scss", "app/controllers/inheritance", "app/controllers/inheritance/api", "app/controllers/inheritance/api/controller.rb", "app/controllers/inheritance/api/sessions.rb", "app/controllers/inheritance/controller_access.rb", "app/controllers/inheritance/controller_base.rb", "app/controllers/inheritance/roles_controller.rb", "app/controllers/inheritance/sessions_controller.rb", "app/helpers/api_helper.rb", "app/helpers/custom_helper.rb", "app/helpers/inheritance_helper.rb", "app/helpers/session_helper.rb", "app/models/concerns/inheritance_api.rb", "app/models/concerns/inheritance_filter.rb", "app/models/concerns/inheritance_permission.rb", "app/models/concerns/inheritance_role.rb", "app/models/concerns/inheritance_user.rb", "app/views/inheritance", "app/views/inheritance/controller_base", "app/views/inheritance/controller_base/_additional_information.html.erb", "app/views/inheritance/controller_base/_form.html.erb", "app/views/inheritance/controller_base/_list.html.erb", "app/views/inheritance/controller_base/edit.html.erb", "app/views/inheritance/controller_base/index.html.erb", "app/views/inheritance/controller_base/new.html.erb", "app/views/inheritance/roles", "app/views/inheritance/roles/_form.html.erb", "app/views/inheritance/roles/edit.html.erb", "app/views/inheritance/roles/new.html.erb", "app/views/inheritance/sessions", "app/views/inheritance/sessions/new.html.erb", "app/views/layouts/sessions.html.erb", "lib/generators", "lib/generators/inheritance", "lib/generators/inheritance/install", "lib/generators/inheritance/install/install_generator.rb", "lib/generators/inheritance/install/templates", "lib/generators/inheritance/install/templates/create_permissions.rb", "lib/generators/inheritance/install/templates/create_roles.rb", "lib/generators/inheritance/install/templates/create_users.rb", "lib/generators/inheritance/install/templates/permission.rb", "lib/generators/inheritance/install/templates/role.rb", "lib/generators/inheritance/install/templates/spec", "lib/generators/inheritance/install/templates/spec/features", "lib/generators/inheritance/install/templates/spec/features/admin", "lib/generators/inheritance/install/templates/spec/features/admin/able_to_manage_inherited_models_spec.rb", "lib/generators/inheritance/install/templates/spec/features/admin/able_to_manage_roles_spec.rb", "lib/generators/inheritance/install/templates/spec/features/admin/able_to_manage_sessions_spec.rb", "lib/generators/inheritance/install/templates/spec/support", "lib/generators/inheritance/install/templates/spec/support/capybara.rb", "lib/generators/inheritance/install/templates/spec/support/database_cleaner.rb", "lib/generators/inheritance/install/templates/spec/support/factory_girl.rb", "lib/generators/inheritance/install/templates/spec/support/helper_methods.rb", "lib/generators/inheritance/install/templates/spec/support/i18n_helper.rb", "lib/generators/inheritance/install/templates/spec/support/inheritance_helper.rb", "lib/generators/inheritance/install/templates/user.rb", "lib/inheritance", "lib/inheritance.rb", "lib/inheritance/engine.rb", "lib/inheritance/inheritance.rb", "lib/inheritance/version.rb", "lib/tasks/inheritance_tasks.rake", "test/dummy", "test/dummy/README.rdoc", "test/dummy/Rakefile", "test/dummy/app", "test/dummy/app/assets", "test/dummy/app/assets/images", "test/dummy/app/assets/javascripts", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/concerns", "test/dummy/app/helpers", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/mailers", "test/dummy/app/models", "test/dummy/app/models/concerns", "test/dummy/app/views", "test/dummy/app/views/layouts", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/bin", "test/dummy/bin/bundle", "test/dummy/bin/rails", "test/dummy/bin/rake", "test/dummy/bin/setup", "test/dummy/config", "test/dummy/config.ru", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers", "test/dummy/config/initializers/assets.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/cookies_serializer.rb", "test/dummy/config/initializers/filter_parameter_logging.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/config/secrets.yml", "test/dummy/lib", "test/dummy/lib/assets", "test/dummy/log", "test/dummy/public", "test/dummy/public/404.html", "test/dummy/public/422.html", "test/dummy/public/500.html", "test/dummy/public/favicon.ico", "test/inheritance_test.rb", "test/test_helper.rb"]
  s.homepage = "http://bitlab.mx"
  s.licenses = ["MIT"]
  s.post_install_message = ""
  s.rubyforge_project = "inheritance-controller"
  s.rubygems_version = "2.5.1"
  s.summary = "InheritanceController and InheritanceAccess"
  s.test_files = ["test/dummy", "test/dummy/README.rdoc", "test/dummy/Rakefile", "test/dummy/app", "test/dummy/app/assets", "test/dummy/app/assets/images", "test/dummy/app/assets/javascripts", "test/dummy/app/assets/javascripts/application.js", "test/dummy/app/assets/stylesheets", "test/dummy/app/assets/stylesheets/application.css", "test/dummy/app/controllers", "test/dummy/app/controllers/application_controller.rb", "test/dummy/app/controllers/concerns", "test/dummy/app/helpers", "test/dummy/app/helpers/application_helper.rb", "test/dummy/app/mailers", "test/dummy/app/models", "test/dummy/app/models/concerns", "test/dummy/app/views", "test/dummy/app/views/layouts", "test/dummy/app/views/layouts/application.html.erb", "test/dummy/bin", "test/dummy/bin/bundle", "test/dummy/bin/rails", "test/dummy/bin/rake", "test/dummy/bin/setup", "test/dummy/config", "test/dummy/config/application.rb", "test/dummy/config/boot.rb", "test/dummy/config/database.yml", "test/dummy/config/environment.rb", "test/dummy/config/environments", "test/dummy/config/environments/development.rb", "test/dummy/config/environments/production.rb", "test/dummy/config/environments/test.rb", "test/dummy/config/initializers", "test/dummy/config/initializers/assets.rb", "test/dummy/config/initializers/backtrace_silencers.rb", "test/dummy/config/initializers/cookies_serializer.rb", "test/dummy/config/initializers/filter_parameter_logging.rb", "test/dummy/config/initializers/inflections.rb", "test/dummy/config/initializers/mime_types.rb", "test/dummy/config/initializers/session_store.rb", "test/dummy/config/initializers/wrap_parameters.rb", "test/dummy/config/locales", "test/dummy/config/locales/en.yml", "test/dummy/config/routes.rb", "test/dummy/config/secrets.yml", "test/dummy/config.ru", "test/dummy/lib", "test/dummy/lib/assets", "test/dummy/log", "test/dummy/public", "test/dummy/public/404.html", "test/dummy/public/422.html", "test/dummy/public/500.html", "test/dummy/public/favicon.ico", "test/inheritance_test.rb", "test/test_helper.rb"]

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 0"])
      s.add_runtime_dependency(%q<bcrypt>, [">= 0"])
      s.add_runtime_dependency(%q<responders>, ["~> 2.0"])
      s.add_runtime_dependency(%q<rails-i18n>, [">= 0"])
      s.add_runtime_dependency(%q<simple_form>, [">= 0"])
      s.add_runtime_dependency(%q<active_model_serializers>, [">= 0"])
    else
      s.add_dependency(%q<rails>, [">= 0"])
      s.add_dependency(%q<bcrypt>, [">= 0"])
      s.add_dependency(%q<responders>, ["~> 2.0"])
      s.add_dependency(%q<rails-i18n>, [">= 0"])
      s.add_dependency(%q<simple_form>, [">= 0"])
      s.add_dependency(%q<active_model_serializers>, [">= 0"])
    end
  else
    s.add_dependency(%q<rails>, [">= 0"])
    s.add_dependency(%q<bcrypt>, [">= 0"])
    s.add_dependency(%q<responders>, ["~> 2.0"])
    s.add_dependency(%q<rails-i18n>, [">= 0"])
    s.add_dependency(%q<simple_form>, [">= 0"])
    s.add_dependency(%q<active_model_serializers>, [">= 0"])
  end
end
